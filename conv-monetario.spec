Name:           conv-monetario
Version:        0.2
Release:        1%{?dist}
Summary:        The "Conversor de Moedas" program from GNU

License:        GPLv3+
URL:            %{name}
Source0:        %{name}/%{name}-%{version}.tar.gz 
BuildArch:      noarch
      
Requires: python
Requires: curl
Requires: jq

%description 
Este programa ira converter um valor dado como argumento de uma moeda          
para outra. ex.: ./conv-monetario USD BRL 10                                    

%prep
%autosetup

%build

%install
mkdir -p %{buildroot}%{_bindir}
cp -f conv-monetario.sh %{buildroot}%{_bindir}
chmod 755 %{buildroot}%{_bindir}/conv-monetario.sh
t

%files
%{_bindir}/conv-monetario.sh

%doc README
%license LICENSE 

%changelog
* Thu May 17 2018 Marcus Carvalho <mvfc00@gmail.com> 0.2-1
- Initial version of the package
- Package used to learn about RPMs
