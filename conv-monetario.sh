#!/bin/bash
if [ -z $1 ]; then
	echo " Formato de execucao deve ser: conv_monetario.sh moeda_origem moeda_destino valor"
	exit 1
elif [ -z $2 ]; then
	echo " Formato de execucao deve ser: conv_monetario.sh moeda_origem moeda_destino valor "	
	exit 1
fi
if [ -z $3 ]; then
	echo " Formato de execucao deve ser: conv_monetario.sh moeda_origem moeda_destino valor " 
	exit 1
fi

if [ ! -f tmp.$1 ]; then
	curl https://exchangeratesapi.io/api/latest?base=$1 2> /dev/null > tmp.$1
fi

VAR02=`cat tmp.$1 | jq .rates.$2`
VAR03=$3

if [ $VAR02 == "null" ]; then
	VARERROR=`cat tmp.$1 | jq .error`
	echo "ERRO: Moeda nao encontrada." 
	exit 1
fi
echo "Moeda Origem.: " $1 
echo "Moeda Destino: " $2 " Valor da unidade: " $VAR02
echo "Valor entrada: " $VAR03 " em " $1
VARFINAL=`python -c 'print( '$VAR02' * '$VAR03')'`
echo "Valor final..: " $VARFINAL " em " $2 
